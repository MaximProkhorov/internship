import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ExchangeService {
    public static void exchange(double amount, Currency from, Currency to) {
        try {
            String url = "https://api.exchangeratesapi.io/latest?base=" + from + "&symbols=" + to;
            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject json = new JSONObject(response.toString());
            JSONObject rates = new JSONObject(json.getJSONObject("rates").toString());
            double exchangeRate = rates.getDouble(to.toString());
            product(1, exchangeRate, from, to);
            product(amount, exchangeRate, from, to);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void product(double fromAmount, double exchangeRate, Currency from, Currency to) {
        System.out.println(fromAmount + " " + from + " = " + exchangeRate*fromAmount + " " + to);
    }
}
