import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class PersonUtil {
    private static List<Person> people = new ArrayList<>();

    static {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Person a1 = null;
        Person a2 = null;
        Person a3 = null;
        Person a4 = null;
        try {
            a1 = new Person("Mmmmmmaxim", "Аркадьев", "Andreevich", 18,
                    Gender.MALE, simpleDateFormat.parse("05-12-1994"));
            a2 = new Person("Bbbbbbbaxim", "Prokhorov", "Andreevich", 20,
                    Gender.MALE, simpleDateFormat.parse("05-11-1994"));
            a3 = new Person("Aaaaaaaxim", "Prokhorov", "Andreevich", 21,
                    Gender.FEMALE, simpleDateFormat.parse("05-12-1994"));
            a4 = new Person("Zzzzzzzzaxim", "грохоров", "Andreevich", 19,
                    Gender.MALE, simpleDateFormat.parse("05-12-1994"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        people.addAll(Arrays.asList(a1,a2,a3,a4,a1,a1,a3,a3,a3,a3,a4));
    }

    public static void match(Person template) {
        System.out.println("\nСовпадения с шаблоном:");
        System.out.println(people.stream().anyMatch(template::equals));
    }

    public static void listOfPeopleOlderThan20AndSurnameStartsWithLetters() {
        System.out.println("\nСписок людей возрастом <=20 с фамилиями на а,б,в,г,д:");
        Set<Character> letters = new HashSet<>(Arrays.asList('а','б','в','г','д'));
        people.stream().filter(p -> p.getAge() <= 20 &&
                letters.contains(p.getSurname().toLowerCase().charAt(0)))
                .distinct()
                .forEach(System.out::println); //a1, a4
    }

    private static void printList(List<?> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + 1 + ": " + list.get(i));
        }
    }

    public static void listOfPeopleRepeatedFrom1to3TimesSortedByAgeDesc() {
        System.out.println("\nОтсортированный до убыванию возраста список людей, повторяющихся от 1 до 3 раз:");
        List<Person> repetitions1to3 = people.stream()
                .filter(p -> Collections.frequency(people, p) > 1)
                .filter(p -> Collections.frequency(people, p) <= 3)
                .distinct()
                .sorted(Comparator.comparing(Person::getAge).reversed())
                .collect(Collectors.toList());
        printList(repetitions1to3); //a4, a1
    }

    public static void listOfPeopleRepeatedMoreThan3TimesSortedByAgeDesc() {
        System.out.println("\nОтсортированный до убыванию возраста список людей, повторяющихся более 3 раз:");
        List<Person> repetitionsMoreThan3 = people.stream()
                .filter(p -> Collections.frequency(people, p) > 3)
                .distinct()
                .sorted(Comparator.comparing(Person::getAge).reversed())
                .collect(Collectors.toList());
        printList(repetitionsMoreThan3); //a3
    }
}
