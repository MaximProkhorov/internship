public class Task2 {
    public static void main(String[] args) {
        ExchangeService.exchange(1337, Currency.USD, Currency.RUB);
        //Output:
        //1.0 USD = 75.8868130943 RUB
        //1337.0 USD = 101460.6691070791 RUB
        ExchangeService.exchange(1337, Currency.EUR, Currency.RUB);
        ExchangeService.exchange(1337, Currency.RUB, Currency.JPY);
    }
}
