import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Task1 {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Person template = new Person("Bbbbbbbaxim", "Prokhorov", "Andreevich", 20,
                Gender.FEMALE, simpleDateFormat.parse("05-11-1994"));
        Person template2 = new Person("Bbbbbbbaxim", "Prokhorov", "Andreevich", 20,
                Gender.MALE, simpleDateFormat.parse("05-11-1994"));
        PersonUtil.match(template); //false
        PersonUtil.match(template2); //true
        PersonUtil.listOfPeopleOlderThan20AndSurnameStartsWithLetters();
        PersonUtil.listOfPeopleRepeatedFrom1to3TimesSortedByAgeDesc();
        PersonUtil.listOfPeopleRepeatedMoreThan3TimesSortedByAgeDesc();
    }
}