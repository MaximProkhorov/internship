import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class Person {
    private String name;
    private String surname;
    private String otchestvo;
    private int age;
    private Gender gender;
    private Date dateOfBirth;
}
